" ----------------------------------------- "
" Pathogen Config 				    		"
" ----------------------------------------- "

let g:pathogen_disabled = []

" LanguageServer only supports vim8 which is not in CentOS
if !has('nvim')
    call add(g:pathogen_disabled, 'LanguageClient-neovim')
endif

execute pathogen#infect()
call pathogen#helptags()


" ----------------------------------------- "
" General Settings 				    		"
" ----------------------------------------- "

set noerrorbells               " No beeps
set number                     " Show line numbers
set backspace=indent,eol,start " Makes backspace key more powerful.
set showcmd                    " Show me what I'm typing
set showmode                   " Show current mode.
set noswapfile                 " Don't use swapfile
set nobackup                   " Don't create annoying backup files
set nowritebackup              " Don't create annoying backup files
set splitright                 " Split vertical windows right to the current windows
set splitbelow                 " Split horizontal windows below to the current windows
set encoding=utf-8             " Set default encoding to UTF-8
set autowrite                  " Automatically save before :next, :make etc.
set autoread                   " Automatically reread changed files without asking me anything
set laststatus=2               " Show status line
set hidden                     " Hide buffers

set ruler          " Show the cursor position all the time
au FocusLost * :wa " Set vim to save the file on focus out.

set fileformats=unix,dos,mac " Prefer Unix over Windows over OS 9 formats

set noshowmatch " Do not show matching brackets by flickering
set noshowmode  " We show the mode with airline or lightline
set incsearch   " Shows the match while typing
set hlsearch    " Highlight found searches
set ignorecase  " Search case insensitive...
set smartcase   " ... but not when search pattern contains upper case characters
set lazyredraw  " Wait to redraw                                                 "

" speed up vim
set nocursorcolumn       " Don't highlight cursor column
set nocursorline         " Don't highlight cursor line
syntax sync minlines=256 " Sync syntax grater then 256 lines
set synmaxcol=300        " Sync out to 300 chars
set re=1                 " Use older regex engine

" do not hide markdown
set conceallevel=0


" open help vertically
command! -nargs=* -complete=help Help vertical belowright help <args>
autocmd FileType help wincmd L

" Make Vim to handle long lines nicely.
set wrap
set textwidth=79
set formatoptions=qrn1

" mutt mail line wrapping
au BufRead /tmp/mutt-* set tw=72

set autoindent  " Indent as definied for filetype
set showmatch   " Show matching bracket
set smarttab    " Smart expanding of tab to space

" Default tab
set tabstop=4    " Tab 4 spaces
set shiftwidth=4 " Indent of auto indenting
set expandtab    " Expand tab to spaces

set shiftround   " Allow shift with Ctrl-T

" Time out on key codes but not mappings.
" Basically this makes terminal Vim work sanely.
set notimeout
set ttimeout
set ttimeoutlen=10

" Better Completion
set complete=.,w,b,u,t
set completeopt=noinsert,menuone,noselect

" Ensure history is 50 if default not set
if &history < 1000
  set history=50
endif

" Ensure tabmx is at least 50
if &tabpagemax < 50
  set tabpagemax=50
endif

" Keep 1 above/bellow and 5 l/r when scrolling if not already set
if !&scrolloff
  set scrolloff=1
endif
if !&sidescrolloff
  set sidescrolloff=5
endif

set display+=lastline " Show as much of end of line as possible

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
  set mouse=a
endif

" If linux then set ttymouse
let s:uname = system("echo -n \"$(uname)\"")
if !v:shell_error && s:uname == "Linux" && !has('nvim')
  set ttymouse=xterm
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
    au!

    " For all text files set 'textwidth' to 78 characters.
    autocmd FileType text setlocal textwidth=78

    " When editing a file, always jump to the last known cursor position.
    " Don't do it when the position is invalid or when inside an event handler
    " (happens when dropping a file on gvim).
    " Also don't do it when the mark is in the first line, that is the default
    " position when opening a file.
    autocmd BufReadPost *
      \ if line("'\"") > 1 && line("'\"") <= line("$") |
      \	exe "normal! g`\"" |
      \ endif

  augroup END
else
endif " has("autocmd")

" This comes first, because we have mappings that depend on leader
" With a map leader it's possible to do extra key combinations
" i.e: <leader>w saves the current file
let mapleader = ","
let g:mapleader = ","

" Dont show me any output when I build something
" Because I am using quickfix for errors
"nmap <leader>m :make<CR><enter>

nnoremap <silent> <leader>q :Sayonara<CR>

" Fast saving
nmap <leader>w :w!<cr>

" Center the screen
nnoremap <space> zz

" Remove search highlight
nnoremap <leader><space> :nohlsearch<CR>

" Better split switching
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Move up and down on splitted lines (on small width screens)
map <Up> gk
map <Down> gj
map k gk
map j gj

" Just go out in insert mode
imap jk <ESC>l

" Set local to spelling
nnoremap <F6> :setlocal spell! spell?<CR>

" Search mappings: These will make it so that going to the next one in a
" search will center on the line it's found in.
nnoremap n nzzzv
nnoremap N Nzzzv

" Act like D and C
nnoremap Y y$

" Do not show stupid q: window
map q: :q

" dont save .netrwhist history
let g:netrw_dirhistmax=0

" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %

" never do this again --> :set paste <ctrl-v> :set no paste
let &t_SI .= "\<Esc>[?2004h"
let &t_EI .= "\<Esc>[?2004l"
inoremap <special> <expr> <Esc>[200~ XTermPasteBegin()

function! XTermPasteBegin()
  set pastetoggle=<Esc>[201~
  set paste
  return ""
endfunction

" set 80 character line limit
if exists('+colorcolumn')
  set colorcolumn=80
else
  au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)
endif


" ----------------------------------------- "
" QuickFix Configs 				    		"
" ----------------------------------------- "

" This trigger takes advantage of the fact that the quickfix window can be
" easily distinguished by its file-type, qf. The wincmd J command is
" equivalent to the Ctrl+W, Shift+J shortcut telling Vim to move a window to
" the very bottom (see :help :wincmd and :help ^WJ).
autocmd FileType qf wincmd J

" Close quickfix easily
nnoremap <leader>a :cclose<CR>

" Some useful quickfix shortcuts
":cc      see the current error
":cn      next error
":cp      previous error
":clist   list all errors
map <leader>cn :cn<CR>
map <leader>cp :cp<CR>


" ----------------------------------------- "
" Buffer Configs 				    		"
" ----------------------------------------- "

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
    \ | wincmd p | diffthis
endif


" Replace the current buffer with the given new file. That means a new file
" will be open in a buffer while the old one will be deleted
com! -nargs=1 -complete=file Breplace edit <args>| bdelete #

function! DeleteInactiveBufs()
  "From tabpagebuflist() help, get a list of all buffers in all tabs
  let tablist = []
  for i in range(tabpagenr('$'))
    call extend(tablist, tabpagebuflist(i + 1))
  endfor

  "Below originally inspired by Hara Krishna Dara and Keith Roberts
  "http://tech.groups.yahoo.com/group/vim/message/56425
  let nWipeouts = 0
  for i in range(1, bufnr('$'))
    if bufexists(i) && !getbufvar(i,"&mod") && index(tablist, i) == -1
      "bufno exists AND isn't modified AND isn't in the list of buffers open in windows and tabs
      silent exec 'bwipeout' i
      let nWipeouts = nWipeouts + 1
    endif
  endfor
  echomsg nWipeouts . ' buffer(s) wiped out'
endfunction

command! Ball :call DeleteInactiveBufs()

" Buffer prev/next
nnoremap <C-x> :bnext<CR>
nnoremap <C-z> :bprev<CR>

"nnoremap <leader>. :lcd %:p:h<CR>
autocmd BufEnter * silent! lcd %:p:h


" ----------------------------------------- "
" File Type settings 			    		"
" ----------------------------------------- "

augroup filetypedetect
  au BufNewFile,BufRead .tmux.conf*,tmux.conf* setf tmux
  au BufNewFile,BufRead .nginx.conf*,nginx.conf* setf nginx
  au BufNewFile,BufRead *.vim,vimrc setf vim
  au BufNewFile,BufRead *.md,*.scd setf markdown
  au BufNewFile,BufRead *.yml,*.yaml setf yaml
  au BufNewFile,BufRead *.cpp,*.hpp setf cpp
  au BufNewFile,BufRead *.json setf json
  au BufNewFile,BufRead *.go setf go
  au BufNewFile,BufRead *.scala setf scala
  au BufNewFile,BufRead MAINTAINERS setf toml
  au BufNewFile,BufRead *.py setf python
  au BufNewFile,BufRead *.ts setf typescript
  au BufNewFile,BufRead *.workflow,*.tf setf hcl
  au BufNewFile,BufRead *.lua setf lua
  au BufNewFile,BufRead Vagrantfile setf ruby
  au BufNewFile,BufRead *.html,*.gohtml setf html
augroup END

""""
" Vim
autocmd FileType vim setlocal et sw=2 sts=2

""""
" Text
autocmd FileType text setlocal noet ts=4 sw=4

""""
" MarkDown
autocmd FileType markdown setlocal
            \ spell noet ts=4 sw=4
function! Heading1()
  normal I# jkA
endfunction
function! Heading2()
  normal I## jkA
endfunction
function! Heading3()
  normal I### jkA
endfunction
nnoremap <leader>h1 :call Heading1()<CR>
nnoremap <leader>h2 :call Heading2()<CR>
nnoremap <leader>h3 :call Heading3()<CR>

""""
" YAML
autocmd FileType yaml setlocal et ts=2 sw=2

""""
" C++
autocmd FileType cpp setlocal et ts=2 sw=2

""""
" JSON
autocmd FileType json setlocal et ts=2 sw=2

""""
" Go
autocmd FileType go setlocal noet ts=4 sw=4 sts=4

""""
" scala
autocmd FileType scala setlocal et ts=2 sw=2

""""
" typescript
autocmd FileType typescript setlocal et ts=4 sw=4

""""
" lua
autocmd FileType lua setlocal noet ts=4 sw=4 sts=4

""""
" Dockerfile
autocmd FileType dockerfile setlocal noet

""""
" shell/config/systemd settings
autocmd FileType fstab,systemd set noet
autocmd FileType gitconfig,sh,toml set noet

""""
" python
autocmd FileType python setlocal et ts=4 sw=4 sts=4 textwidth=80 smarttab

""""
" git commits
autocmd FileType gitcommit setlocal spell

""""
" html
autocmd FileType html setlocal et ts=2 sw=2 sts=2

" ----------------------------------------- "
" Plugin configs 			    			"
" ----------------------------------------- "

" ========== Fugitive ==========

nnoremap <leader>ga :Git add %:p<CR><CR>
nnoremap <leader>gs :Gstatus<CR>
nnoremap <leader>gp :Gpush<CR>
vnoremap <leader>gb :Gblame<CR>


" ========== vim-cfmt ==========

let g:cfmt_style = '-linux'


" ========== vim-fzf ==========

set rtp+=/usr/local/opt/fzf


" ========== Vim-go ==========

let g:go_fmt_fail_silently = 0
let g:go_fmt_command = "goimports"
let g:go_autodetect_gopath = 0
let g:go_term_enabled = 1
let g:go_snippet_engine = "neosnippet"
let g:go_highlight_space_tab_error = 0
let g:go_highlight_array_whitespace_error = 0
let g:go_highlight_trailing_whitespace_error = 0
let g:go_highlight_extra_types = 0
let g:go_highlight_operators = 0
let g:go_highlight_build_constraints = 1
let g:go_fmt_autosave = 1
let g:go_version_warning = 0 " Have to use old vim for CentOS
let g:go_doc_keywordprg_enabled = 0

au FileType go nmap <Leader>gd <Plug>(go-def)
au FileType go nmap <Leader>s <Plug>(go-def-split)
au FileType go nmap <Leader>v <Plug>(go-def-vertical)
au FileType go nmap <Leader>i <Plug>(go-info)
au FileType go nmap <Leader>l <Plug>(go-metalinter)

au FileType go nmap <leader>r  <Plug>(go-run)

au FileType go nmap <leader>b  <Plug>(go-build)
au FileType go nmap <leader>t  <Plug>(go-test)
au FileType go nmap <leader>dt  <Plug>(go-test-compile)
au FileType go nmap <Leader>d <Plug>(go-doc)

au FileType go nmap <Leader>e <Plug>(go-rename)

" neovim specific
if has('nvim')
  au FileType go nmap <leader>rt <Plug>(go-run-tab)
  au FileType go nmap <Leader>rs <Plug>(go-run-split)
  au FileType go nmap <Leader>rv <Plug>(go-run-vertical)
endif

" Alternate to test_*.go file
augroup go
  autocmd!
  autocmd Filetype go command! -bang A call go#alternate#Switch(<bang>0, 'edit')
  autocmd Filetype go command! -bang AV call go#alternate#Switch(<bang>0, 'vsplit')
  autocmd Filetype go command! -bang AS call go#alternate#Switch(<bang>0, 'split')
augroup END


" ========== delimitMate ==========
let g:delimitMate_expand_cr = 1
let g:delimitMate_expand_space = 1
let g:delimitMate_smart_quotes = 1
let g:delimitMate_expand_inside_quotes = 0
let g:delimitMate_smart_matchpairs = '^\%(\w\|\$\)'


" ========== NerdTree ==========

" For toggling
noremap <Leader>n :NERDTreeToggle<cr>
noremap <Leader>f :NERDTreeFind<cr>

let NERDTreeShowHidden=1

let NERDTreeIgnore=['\.vim$', '\~$', '\.git$', '.DS_Store']

" Close nerdtree and vim on close file
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif


" ========== vim-json ==========

let g:vim_json_syntax_conceal = 0


" ========== LanguageClient ==========

let g:LanguageClient_serverCommands = {}

if executable('clangd')
  let g:LanguageClient_serverCommands.c = ['clangd']
  autocmd FileType c setlocal omnifunc=LanguageClient#complete
endif

if executable('gopls')
  let g:LanguageClient_serverCommands.go = ['gopls']
  autocmd FileType go setlocal omnifunc=LanguageClient#complete
endif

if executable('pyls')
  let g:LanguageClient_serverCommands.python = ['pyls']
  autocmd FileType python setlocal omnifunc=LanguageClient#complete
endif

if executable('rustup')
  let g:LanguageClient_serverCommands.rust = ['rustup']
  autocmd FileType rust setlocal omnifunc=LanguageClient#complete
endif

if executable('javascript-typescript-stdio')
  let g:LanguageClient_serverCommands.typescript = ['javascript-typescript-stdio']
  autocmd FileType typescript setlocal omnifunc=LanguageClient#complete
endif

if executable('bash-language-server')
  let g:LanguageClient_serverCommands.bash = ['bash-language-server']
  autocmd FileType bash setlocal omnifunc=LanguageClient#complete
endif

if executable('clojure-lsp')
  let g:LanguageClient_serverCommands.clojure = ['clojure-lsp']
  autocmd FileType clojure setlocal omnifunc=LanguageClient#complete
endif

if executable('zig-lsp')
  let g:LanguageClient_serverCommands.zig = ['zig-lsp']
  autocmd FileType zig setlocal omnifunc=LanguageClient#complete
endif

let g:LanguageClient_autoStart = 1
let g:LanguageClient_diagnosticsList = 'Disabled'
" let g:LanguageClient_diagnosticsEnable = 0
let g:LanguageClient_loadSettings = 1

nnoremap <F5> :call LanguageClient_contextMenu()<CR>
nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>


" ========== ncm2/mucomplete ==========

if has('nvim')
  autocmd BufEnter * call ncm2#enable_for_buffer()
else
  let g:mucomplete#enable_auto_at_startup = 1
  let g:mucomplete#chains = {}
  let g:mucomplete#chains.default  = ['path', 'omni', 'keyn', 'dict', 'uspl', 'ulti']
  let g:mucomplete#chains.markdown = ['path', 'keyn', 'dict', 'uspl']
  let g:mucomplete#chains.vim      = ['path', 'keyn', 'dict', 'uspl']
endif



" ========== vim-multiple-cursors ==========

" Maybe remove, causes many issues
let g:multi_cursor_use_default_mapping=0
let g:multi_cursor_next_key='<C-i>'
let g:multi_cursor_prev_key='<C-y>'
let g:multi_cursor_skip_key='<C-b>'
let g:multi_cursor_quit_key='<Esc>'

" Called once right before you start selecting multiple cursors
function! Multiple_cursors_before()
  if exists(':NeoCompleteLock')==2
    exe 'NeoCompleteLock'
  endif
endfunction

" Called once only when the multiple selection is canceled (default <Esc>)
function! Multiple_cursors_after()
  if exists(':NeoCompleteUnlock')==2
    exe 'NeoCompleteUnlock'
  endif
endfunction


" ========== vim-better-whitespace ==========

" auto strip whitespace except for file with extention blacklisted
let blacklist = ['diff', 'gitcommit', 'unite', 'qf', 'help', 'markdown']
autocmd BufWritePre * if index(blacklist, &ft) < 0 | StripWhitespace


" ========= vim-markdown ==================

" disable folding
let g:vim_markdown_folding_disabled = 1

" Allow for the TOC window to auto-fit when it's possible for it to shrink.
" It never increases its default size (half screen), it only shrinks.
let g:vim_markdown_toc_autofit = 1

" Disable conceal
let g:vim_markdown_conceal = 0

" Allow the ge command to follow named anchors in links of the form
" file#anchor or just #anchor, where file may omit the .md extension as usual
let g:vim_markdown_follow_anchor = 1

" highlight frontmatter
let g:vim_markdown_frontmatter = 1
let g:vim_markdown_toml_frontmatter = 1
let g:vim_markdown_json_frontmatter = 1


" =================== lightline ========================

let g:lightline = {
      \ 'colorscheme': 'seoul256',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'gitbranch#name'
      \ },
      \ }


" =================== rust.vim ========================

" Enable automatic running of :RustFmt when a buffer is saved.
let g:rustfmt_autosave = 1

" The :RustPlay command will send the current selection, or if nothing is
" selected the current buffer, to the Rust playpen. Then copy the url to the
" clipboard.
let g:rust_clip_command = 'xclip -selection clipboard'


" =================== vim-terraform ========================

" Allow vim-terraform to override your .vimrc indentation syntax for matching files.
let g:terraform_align=1

" Run terraform fmt on save.
let g:terraform_fmt_on_save=1


" ----------------------------------------- "
" Unsure of        				    		"
" ----------------------------------------- "

" trim all whitespaces away
nnoremap <leader>W :%s/\s\+$//<cr>:let @/=''<CR>
